﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace MailLoginUI
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        // This function handles the privacy option click radio button
        private void PrivacyOption_Click(object sender, RoutedEventArgs e)
        {
            RadioButton radio = sender as RadioButton;
            if (radio.Tag.ToString().Equals("Warning")) {
                WarningMessage.Visibility = System.Windows.Visibility.Visible;
            }
            else if (radio.Tag.ToString().Equals("RemoveWarning")) {
                WarningMessage.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        // This function handles the sign in button. It handles the click event on it 
        private void SigninButton_Click(object sender, RoutedEventArgs e)
        {
            if ((UsernameField.Text.Length == 0) || (PasswordField.Password.Length == 0))
            {
                MessageBox.Show("Invalid username and/or password");
            }
            else {
                MessageBox.Show("Valid login");
            }
        }
    }
}